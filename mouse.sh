mouse() {
    PS3='Choose parameter mouse to change the value: '
    options=("Resolution" "Rate" "Back")
    select opx in "${options[@]}"

    do
        case $opx in
            "Resolution")
                read -p "Set resolution value (25 - 200): " res
                if  [[ ( $res -ge 25 && $res -le 200) ]]
                then
                    sudo -S modprobe -r psmouse
                    sudo -S modprobe psmouse resolution=$res
                    #echo --- resolution : $(cat /sys/bus/serio/drivers/psmouse$
                    echo 'Your changes is success!'
                else
                    echo 'Your resolution is out of range'
                fi
            bash mouse.sh
            ;;
            "Rate")
                read -p "Set rate value: " rate
                if  [[ ( $rate -ge 0 && $rate -le 100) ]]
                then
                    sudo modprobe -r psmouse
                    sudo modprobe psmouse rate=$rate
                    echo 'Your changes is success!'
                else
                    echo 'Your rate is out of range'
                fi
            bash mouse.sh
            ;;
            "Back")
                bash main.sh
                exit
            ;;
            *)
        esac
    done
}

mouse