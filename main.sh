main() {

    echo '==========================================='
    echo 'Welcome to OASE'
    echo '==========================================='

    COLUMNS=12
    PS3='Choose device driver to control: '
    options=("Ethernet" "Mouse" "Webcam" "Exit Script")
    select opt in "${options[@]}"
    do
        case $opt in
            "Ethernet")
                bash ethernet.sh
                exit
                ;;
            "Mouse")
                bash mouse.sh
                exit
            "Webcam")
                bash webcam.sh
                exit
                ;;
            "Exit Script")
                echo 'Exiting Script'
                exit
                ;;
            *)
        esac
    done
}

main
