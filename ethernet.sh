ethernet() {
    PS3='Choose parameter ethernet to configure: '
    options=("Copybreak" "Back")
    select opt in "${options[@]}"
    do
        case $opt in
            "Copybreak")
                header
                set_cb
                ;;
            "Back")
                echo 'Back to Menu'
                bash main.sh
                exit
                ;;
            *)
        esac
    done
}

header() {
        echo '=====DEVICE INFORMATION====='
        echo 'Parameter default value :'
        echo '--- copybreak : 256'
        echo 'Current paremeter value :'
        echo --- copybreak : $(cat /sys/module/e1000/parameters/copybreak)
        echo '============================'

}
set_cb() {
        read -p 'Set Copybreak value: ' cb
        sudo -S modprobe -r e1000
        sudo -S modprobe e1000 copybreak=$cb
        header
        bash ethernet.sh
}

ethernet